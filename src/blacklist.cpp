#include "blacklist.h"
#include <utils.h>

std::string get_main_domain(const std::string &domain) {
    std::vector<std::string> subdomains = split(domain, '.');
    std::string main_domain = subdomains.at(subdomains.size() - 1) + "." + subdomains.back();
    return main_domain;
}

void BlackList::add(const std::string &domain) {
    blacklist_.insert(get_main_domain(domain));
}

bool BlackList::is_blacklisted(const std::string &domain) {
    if (blacklist_.find(get_main_domain(domain)) != blacklist_.end()) {
        return true;
    }
    return false;
}
