#include "utils.h"

std::vector<std::string> split(const std::string &str, char sep) {
    std::vector<std::string> result;
    size_t last = 0;
    size_t next = 0;
    while ((next = str.find(sep, last)) != std::string::npos) {
        result.push_back(str.substr(last, next - last));
        last = next + 1;
    }
    result.push_back(str.substr(last));
    return result;
}
