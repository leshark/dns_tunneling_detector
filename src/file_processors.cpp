#include <string>
#include <vector>
#include <experimental/filesystem>
#include <fstream>
#include <iomanip>
#include <cstdio>
#include <iterator>
#include <set>

#include "file_processors.h"
#include "utils.h"
#include "spdlog/spdlog.h"

namespace fs = std::experimental::filesystem;
using json = nlohmann::json;

const std::set<std::string> SUPPORTED_EXT = {".pcap", ".dmp", ".pcapng", ".cap"};
const char CSV_SEP = ',';

std::vector<std::string> get_pcaps(const std::string &directory) {
    std::vector<std::string> pcaps;
    for (auto &file : fs::recursive_directory_iterator(directory)) {
        if (SUPPORTED_EXT.find(file.path().extension()) != SUPPORTED_EXT.end() &&
            file.path().string().find("_parsed") == std::string::npos) {
            pcaps.push_back(file.path());
        }
    }
    return pcaps;
}

std::string get_pcap_filename(const std::string &pcap_path) {
    std::string pcap_filename = split(pcap_path, fs::path::preferred_separator).back();
    return pcap_filename;
}

void write_statistics(const json &stats, const std::string &directory) {
    auto file = fs::path(directory) / "stats.json";
    std::ofstream stats_file(file);
    if (stats_file.is_open()) {
        stats_file << std::setw(4) << stats << std::endl;
        stats_file.close();
    } else {
        spdlog::error("unable to write statistics to output directory: {}", directory);
    }
}

void write_results(const std::vector<parsed_dns_packet> &results, const std::string &directory) {
    auto file = fs::path(directory) / "results.csv";
    std::ofstream result_file(file, std::ios::app);
    if (result_file.is_open()) {
        for (const auto &dns_tunnel: results) {
            std::stringstream csv_row;
            csv_row <<
                    dns_tunnel.pcap_path << CSV_SEP <<
                    dns_tunnel.frame_number << CSV_SEP <<
                    dns_tunnel.is_tunnel << CSV_SEP <<
                    dns_tunnel.reason << CSV_SEP <<
                    dns_tunnel.probability << std::endl;
            result_file << csv_row.str();
        }
        result_file.close();
    } else {
        spdlog::error("unable to write results to output directory: {}", directory);
    }
}

void mark_pcap_as_read(const std::string &pcap_path) {
    std::ifstream pcap_file(pcap_path);
    if (pcap_file.good()) {
        auto paths = split(pcap_path, fs::path::preferred_separator);
        std::string filename = paths.back();

        auto filename_parts = split(filename, '.');
        std::string name = filename_parts.front();
        std::string ext = filename_parts.back();

        paths.pop_back();

        paths.push_back(name + "_parsed." + ext);

        std::ostringstream joined_path;
        std::copy(paths.begin(), paths.end(), std::ostream_iterator<std::string>(joined_path, preferred_separator));

        auto new_path = joined_path.str().substr(0, joined_path.str().size() - 1);

        std::rename(pcap_path.c_str(), new_path.c_str());
    } else {
        spdlog::error("cannot rename file: {}", pcap_path);
    }
}