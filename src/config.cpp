#include <fstream>
#include <sstream>
#include <algorithm>
#include "config.h"
#include "spdlog/spdlog.h"

std::map<std::string, std::string> read_config(const std::string &filename) {
    std::map<std::string, std::string> config;

    std::ifstream config_file(filename);
    if (config_file.good()) {
        std::string config_contents((std::istreambuf_iterator<char>(config_file)),
                                    std::istreambuf_iterator<char>());

        std::istringstream config_stream(config_contents);

        std::string line;
        while (std::getline(config_stream, line)) {
            std::istringstream line_stream(line);
            std::string key;
            if (std::getline(line_stream, key, '=')) {
                std::string value;
                if (std::getline(line_stream, value)) {
                    value.erase(std::remove(value.begin(), value.end(), '\r'), value.end());
                    config[key] = value;
                }
            }
        }

    } else {
        spdlog::error("Config file by this path: {} not found", filename);
        exit(1);
    }
    config_file.close();
    return config;
}
