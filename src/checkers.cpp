#include <string>
#include <algorithm>
#include "checkers.h"
#include "utf8.h"
#include <regex>
#include <set>
#include <cmath>

#include "utils.h"


const std::regex BAD_SYMBOLS("_/\\\\");
const std::regex HEX_PATTERN("[A-Fa-f0-9]+");


bool check_label_length(const std::string &domain_name, int threshold /*40*/) {
    auto labels = split(domain_name, '.');
    // check if any of labels is >= threshold
    if (std::any_of(labels.cbegin(), labels.cend(), [&](auto label) { return label.size() > threshold; }))
        return true;
    return false;
}

bool check_utf8_encoding(const std::string &domain_name) {
    return !(utf8::is_valid(domain_name.begin(), domain_name.end()));
}

bool check_bad_symbols(const std::string &domain_name) {
    if (std::regex_search(domain_name, BAD_SYMBOLS)) {
        return true;
    }
    return false;
}

bool check_shannon_entropy(const std::string &domain_name, double threshold /*4.5*/) {
    int size = domain_name.size();
    std::set<char> name_set;
    double entropy_value = 0;

    name_set.insert(domain_name.begin(), domain_name.end());
    for (const auto &symbol : name_set) {
        double symbol_count = std::count(domain_name.begin(), domain_name.end(), symbol);
        auto prob = std::log2(symbol_count / size) * (symbol_count / size);
        entropy_value += prob;
    }

    if (-entropy_value >= threshold) {
        return true;
    }

    return false;
}

bool check_hex_symbols(const std::string &domain_name, int threshold) {
    std::cmatch matched_hex;
    std::regex_search(domain_name.c_str(), matched_hex, HEX_PATTERN);
    if (!matched_hex.empty() && matched_hex.size() > threshold) {
        return true;
    }
    return false;
}
