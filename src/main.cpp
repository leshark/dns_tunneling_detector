#include "whitelist.h"
#include "checkers.h"
#include "file_processors.h"
#include <string>
#include <map>
#include <netinet/in.h>
#include <chrono>
#include "IPv4Layer.h"
#include "Packet.h"
#include "PcapFileDevice.h"
#include "DnsLayer.h"
#include "IPv6Layer.h"

#include "config.h"
#include "dns.h"
#include "whitelist.h"
#include "blacklist.h"

#include "spdlog/spdlog.h"

using json = nlohmann::json;

config CONFIG;
std::map<std::string, std::map<std::string, int>> SESSION_STATS;
WhiteList WHITE_LIST;
BlackList BLACK_LIST;
int invalid_packets_count = 0;
long total_time;

std::vector<pcpp::RawPacket> pcap_to_packets(const std::string &pcap_path) {

    std::vector<pcpp::RawPacket> pcap_packets;
    pcpp::PcapFileReaderDevice pcap_file(pcap_path.c_str());

    if (!pcap_file.open()) {
        spdlog::error("Error opening file: {}", pcap_path);
    }

    pcpp::RawPacket rawPacket;
    while (pcap_file.getNextPacket(rawPacket)) {
        pcap_packets.push_back(rawPacket);
    }

    pcap_file.close();
    return pcap_packets;
}

std::vector<dns_packet> parse_packets(const std::vector<pcpp::RawPacket> &packets, const std::string &pcap_path) {
    std::vector<dns_packet> dns_packets;
    int frame_number = 0;
    for (auto raw_packet : packets) {
        frame_number += 1;
        pcpp::Packet parsed_packet(&raw_packet);

        // skip if packet is not DNS request
        if (!parsed_packet.isPacketOfType(pcpp::DNS)) {
            spdlog::info("Not DNS packet found in: {}, frame number: {}", pcap_path, frame_number);
            invalid_packets_count += 1;
            continue;
        }

        auto *dns_layer = parsed_packet.getLayerOfType<pcpp::DnsLayer>();

        // skip DNS requests with more than 1 request or with 0 requests
        if (dns_layer->getDnsHeader()->numberOfQuestions != htons(1) || dns_layer->getFirstQuery() == nullptr) {
            spdlog::info("Invalid DNS packet found in: {}, frame number: {}", pcap_path, frame_number);
            invalid_packets_count += 1;
            continue;
        }

        pcpp::DnsQuery *dns_query = dns_layer->getFirstQuery();

        // make output pretty
        std::string pcap_filename = get_pcap_filename(pcap_path);

        dns_packets.push_back({pcap_filename, frame_number, *dns_query});
    }
    return dns_packets;
}

std::vector<parsed_dns_packet> detect_tunnels(const std::vector<dns_packet> &dns_packets) {
    std::vector<parsed_dns_packet> results;
    int dns_tunnels = 0;
    std::string current_pcap = dns_packets[0].pcap_path;
    for (auto &dns_packet: dns_packets) {

        std::string domain_name = dns_packet.dns_query.getName();

        //start checkers
        if (!WHITE_LIST.is_empty && WHITE_LIST.domain_in_whitelist(domain_name)) {
            continue;
        } else if (BLACK_LIST.is_blacklisted(domain_name)) {
            results.push_back(
                    {dns_packet.pcap_path, dns_packet.frame_number, true, "domain was blacklisted", "high"});
            dns_tunnels += 1;
        } else if (check_utf8_encoding(domain_name)) {
            results.push_back(
                    {dns_packet.pcap_path, dns_packet.frame_number, true, "invalid encoding detected", "100%"});
            dns_tunnels += 1;
            BLACK_LIST.add(domain_name);
        } else if (check_label_length(domain_name)) {
            results.push_back(
                    {dns_packet.pcap_path, dns_packet.frame_number, true, "packet label length too high", "medium"});
            dns_tunnels += 1;
        } else if (check_bad_symbols(domain_name)) {
            results.push_back(
                    {dns_packet.pcap_path, dns_packet.frame_number, true, "invalid symbols detected", "high"});
            dns_tunnels += 1;
            BLACK_LIST.add(domain_name);
        } else if (check_shannon_entropy(domain_name)) {
            results.push_back(
                    {dns_packet.pcap_path, dns_packet.frame_number, true, "domain name entropy exceeded threshold",
                     "medium"});
            dns_tunnels += 1;
        } else if (check_hex_symbols(domain_name)) {
            results.push_back(
                    {dns_packet.pcap_path, dns_packet.frame_number, true, "long hex value detected", "medium"});
            dns_tunnels += 1;
        }
    }

    SESSION_STATS[current_pcap] = {{"dns_tunnels_count", dns_tunnels},
                                   {"packets_count",     dns_packets.size()}};

    return results;
}

json compute_statistics() {
    json stats;
    long total_valid_packets = 0;
    long total_dns_tunnels = 0;
    for (auto &[pcap_path, packet_stats] : SESSION_STATS) {
        total_valid_packets += packet_stats["packets_count"];
        total_dns_tunnels += packet_stats["dns_tunnels_count"];

        stats[pcap_path]["packets_count"] = packet_stats["packets_count"];
        stats[pcap_path]["dns_tunnels_count"] = packet_stats["dns_tunnels_count"];
    }
    stats["total_packets"] = total_valid_packets + invalid_packets_count;
    stats["total_dns_tunnels"] = total_dns_tunnels;
    stats["invalid_packets_count"] = invalid_packets_count;
    stats["packets_per_second"] = (total_valid_packets / total_time) + (invalid_packets_count / total_time);

    return stats;
}


int main() {
    auto start_time = std::chrono::high_resolution_clock::now();

    auto parsed_config = read_config("../conf/options.config");
    CONFIG.input_dir = parsed_config["pcap_dir"];
    CONFIG.output_dir = parsed_config["output_dir"];
    CONFIG.whitelist_path = parsed_config["whitelist_path"];
    auto pcaps = get_pcaps(CONFIG.input_dir);

    if (pcaps.empty()) {
        spdlog::error("No traffic files found in input directory!");
        exit(1);
    }

    WHITE_LIST = WhiteList(CONFIG.whitelist_path);

    spdlog::info("started files processing...");

    for (auto &pcap : pcaps) {
        auto raw_packets = pcap_to_packets(pcap);
        auto dns_queries = parse_packets(raw_packets, pcap);
        auto results = detect_tunnels(dns_queries);

        write_results(results, CONFIG.output_dir);
        //mark_pcap_as_read(pcap);
    }

    spdlog::info("Finished files processing.");

    auto end_time = std::chrono::high_resolution_clock::now();
    total_time = std::chrono::duration_cast<std::chrono::seconds>(end_time - start_time).count();

    auto json_stats = compute_statistics();
    write_statistics(json_stats, CONFIG.output_dir);

    return 0;
}