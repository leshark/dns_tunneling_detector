#include <fstream>
#include "whitelist.h"
#include <iostream>
#include <sstream>
#include <iterator>
#include "utils.h"
#include "spdlog/spdlog.h"

WhiteList::WhiteList(const std::string &filename) {
    std::ifstream whitelist(filename);
    std::string domain;
    if (whitelist.good()) {
        while (getline(whitelist, domain)) {
            // trim unnecessary symbols
            domain.erase(domain.find_last_not_of(" \n\r\t") + 1);
            if (!domain.empty()) {
                domains_.push_back(domain);
            }
        }
        whitelist.close();
    } else {
        spdlog::error("unable to open whitelist file: {}", filename);
        exit(1);
    }
    parse_domains();
}

void WhiteList::parse_domains() {
    if (domains_.empty()) {
        spdlog::info("whitelist file is empty!");
        is_empty = true;
    }
    for (const auto &domain_str: domains_) {
        whitelist_.insert(domain_str);
    }
}

bool WhiteList::domain_in_whitelist(const std::string &domain) {
    std::vector<std::string> subdomains = split(domain, '.');

    for (int i = 0; i < subdomains.size(); i++) {
        std::ostringstream joined_domain;
        const char *delim = ".";
        // advance +i to try all subdomains - wildcard matching (a.b.com -> b.com -> com)
        std::copy(subdomains.begin() + i, subdomains.end(), std::ostream_iterator<std::string>(joined_domain, delim));
        auto shortened_domain = joined_domain.str().substr(0, joined_domain.str().size() - 1);

        if (whitelist_.find(shortened_domain) != whitelist_.end()) {
            return true;
        }
    }
    return false;
}

