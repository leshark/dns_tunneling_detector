#ifndef DNS_TUNELLING_DETECTOR_FILE_PROCESSORS_H
#define DNS_TUNELLING_DETECTOR_FILE_PROCESSORS_H

#include <vector>
#include <string>
#include "json.hpp"

#include "dns.h"

#ifdef _GLIBCXX_FILESYSTEM_IS_WINDOWS
static const char* preferred_separator = "\\";
#else
static const char* preferred_separator = "/";
#endif

using json = nlohmann::json;

std::vector<std::string> get_pcaps(const std::string &directory);

std::string get_pcap_filename(const std::string &pcap_path);

void write_statistics(const json &stats, const std::string &directory);

void write_results(const std::vector<parsed_dns_packet> &results, const std::string &directory);

void mark_pcap_as_read(const std::string &pcap_path);

#endif //DNS_TUNELLING_DETECTOR_FILE_PROCESSORS_H
