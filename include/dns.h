#ifndef DNS_TUNELLING_DETECTOR_DNS_H
#define DNS_TUNELLING_DETECTOR_DNS_H

#include <string>
#include "DnsLayer.h"

/// Структура для хранения первичных результатов парсинга файла трафика
struct dns_packet {
    std::string pcap_path;
    int frame_number;
    pcpp::DnsQuery dns_query;
};

/// Структура для хранения финальных результатов парсинга файла трафика
struct parsed_dns_packet {
    std::string pcap_path;
    int frame_number;
    bool is_tunnel;
    std::string reason;
    std::string probability;
};

#endif //DNS_TUNELLING_DETECTOR_DNS_H
