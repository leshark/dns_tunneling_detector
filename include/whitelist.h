#ifndef DNS_TUNELLING_DETECTOR_WHITELIST_H
#define DNS_TUNELLING_DETECTOR_WHITELIST_H

#include <set>
#include <string>
#include <vector>
#include "utils.h"

/// Класс белого списка
class WhiteList {
public:

    WhiteList() = default;

    explicit WhiteList(const std::string &filename);

    bool domain_in_whitelist(const std::string &domain);
    /// Функция, определяющая находится ли домен в белом списке

    bool is_empty = false;

private:
    void parse_domains();

    std::vector<std::string> domains_;
    std::set<std::string> whitelist_;
};

#endif //DNS_TUNELLING_DETECTOR_WHITELIST_H
