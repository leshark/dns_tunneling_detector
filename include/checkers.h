#ifndef DNS_TUNELLING_DETECTOR_CHECKERS_H
#define DNS_TUNELLING_DETECTOR_CHECKERS_H

#include <string>

bool check_label_length(const std::string &domain_name, int threshold = 40);

bool check_utf8_encoding(const std::string &domain_name);

bool check_bad_symbols(const std::string &domain_name);

bool check_shannon_entropy(const std::string &domain_name, double threshold = 4.5);

bool check_hex_symbols(const std::string &domain_name, int threshold = 20);

#endif //DNS_TUNELLING_DETECTOR_CHECKERS_H
