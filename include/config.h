#ifndef DNS_TUNELLING_DETECTOR_CONFIG_H
#define DNS_TUNELLING_DETECTOR_CONFIG_H

#include <string>
#include <map>

/// Структура для хранения результатов парсинга конфигурационного файла
struct config {
    std::string input_dir;
    std::string output_dir;
    std::string whitelist_path;
};

std::map<std::string, std::string> read_config(const std::string &filename);

#endif //DNS_TUNELLING_DETECTOR_CONFIG_H
