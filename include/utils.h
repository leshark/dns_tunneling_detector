#ifndef DNS_TUNELLING_DETECTOR_UTILS_H
#define DNS_TUNELLING_DETECTOR_UTILS_H

#include <string>
#include <vector>

std::vector<std::string> split(const std::string &str, char sep);

#endif //DNS_TUNELLING_DETECTOR_UTILS_H
