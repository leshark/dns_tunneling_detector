#ifndef DNS_TUNELLING_DETECTOR_BLACKLIST_H
#define DNS_TUNELLING_DETECTOR_BLACKLIST_H

#include <string>
#include <set>

/// Класс черного списка
class BlackList {
public:

    BlackList() = default;

    bool is_blacklisted(const std::string &domain);
    /// Функция, определяющая находится ли домен в черном списке

    void add(const std::string &domain);
    /// Функци добавления домена в черный список

private:
    std::set<std::string> blacklist_;
};

#endif //DNS_TUNELLING_DETECTOR_BLACKLIST_H
